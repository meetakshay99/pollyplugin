// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TruPolly.h"
#include <Components/AudioComponent.h>
#include "GameFramework/Actor.h"
#include "PollyActor.generated.h"

UCLASS()
class PLUGINTEST_API APollyActor : public AActor
{
    GENERATED_BODY()
    
private:
    TSharedPtr<TruPolly> pollyPtr;
    UAudioComponent *audioComponent;
    
    static void logHandler(const char *text);
    void logError(const char *msg, const char *error);
    
public:
    // Sets default values for this actor's properties
    APollyActor();
    
protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
    
    
    
};
