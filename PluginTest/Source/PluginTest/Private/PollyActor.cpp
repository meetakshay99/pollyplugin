// Fill out your copyright notice in the Description page of Project Settings.

#include "PollyActor.h"
#include "TruPollyLogger.h"
#include <Sound/SoundWaveProcedural.h>
#include <aws/text-to-speech/TextToSpeechManager.h>

Aws::TextToSpeech::CapabilityInfo m_selectedCaps;

// Sets default values
APollyActor::APollyActor()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    
}

// Called when the game starts or when spawned
void APollyActor::BeginPlay()
{
    Super::BeginPlay();
    
    TruPollyLogger::setLogHandler(APollyActor::logHandler);
    
    pollyPtr = MakeShared<TruPolly>();
    audioComponent = FindComponentByClass<UAudioComponent>();

    char sampleRate[256];
    snprintf(sampleRate, sizeof sampleRate, "%zu", m_selectedCaps.sampleRate);
    UE_LOG(LogTemp, Log, TEXT("sample Rate = %s"), sampleRate);
    pollyPtr->setSampleRate(sampleRate);
    pollyPtr->setOutputFormat(Aws::Polly::Model::OutputFormat::pcm);

    UE_LOG(LogTemp, Log, TEXT("Before making a call to getPollyData"));
    pollyPtr->getPollyData("This is sample test of polly plugin", false, [this](const char* text, const std::vector<unsigned char> audioData, const char* error) mutable {
        UE_LOG(LogTemp, Log, TEXT("Got Polly Response in actor"));
        if (strcmp(error,"") != 0) {
            logError("Received error while performing Polly request", error);
        } else {
            const uint8 *reqdAudioData = audioData.data();
    //        const unsigned char *audioDataChar = reinterpret_cast<unsigned char*>(const_cast<unsigned char*>(audioData.data()));
    //
    //        const uint8 *reqdAudioData = static_cast<const uint8*>(audioDataChar);
            
            USoundWaveProcedural *audio = NewObject<USoundWaveProcedural>();
            audio->SetSampleRate((int32)m_selectedCaps.sampleRate); //(22050);
            audio->NumChannels = 1;
            audio->Duration = INDEFINITELY_LOOPING_DURATION;
            audio->SoundGroup = SOUNDGROUP_Voice;
            audio->bLooping = false;
            audio->QueueAudio(reqdAudioData, audioData.size());

            AsyncTask(ENamedThreads::GameThread, [audio, this]() {
                // code to execute on game thread here
                
                // Get the audio component and set audio to be played.
                this->audioComponent->SetSound((USoundBase*)audio);
                
                // Activate.
                this->audioComponent->Play();
                
                UE_LOG(LogTemp, Log, TEXT("Played audio via component"));
                
            });
        }
    });
    UE_LOG(LogTemp, Log, TEXT("After making a call to getPollyData"));
}

void APollyActor::logError(const char *msg, const char *error) {
    UE_LOG(LogTemp, Log, TEXT("%s : %s"), *FString(UTF8_TO_TCHAR(msg)), *FString(UTF8_TO_TCHAR(error)));
}

void APollyActor::logHandler(const char *text) {
    UE_LOG(LogTemp, Log, TEXT("%s"), *FString(UTF8_TO_TCHAR(text)));
}

// Called every frame
void APollyActor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
}

