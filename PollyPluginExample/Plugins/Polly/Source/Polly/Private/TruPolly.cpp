// Fill out your copyright notice in the Description page of Project Settings.

#include "TruPolly.h"
#include <iostream>
#include <exception>
#include <typeinfo>
#include <stdexcept>
#include "TruPollyInternal.h"
#include <stdio.h>
#include <sstream>
#include "TruPollyLogger.h"

using std::cout;
using namespace std;
using namespace Aws::Polly;

TruPolly::TruPolly() {
    ppInternal = new TruPollyInternal();
}

void TruPolly::getPollyData(const char *text, bool isSSML, PollyDataHandler handler) {
    TextType textType;
    bool notified = false;
    bool audioResponseRecvd = false;
    bool speechMarksResponseRecvd = false;
    std::vector<unsigned char> audioData;
    std::vector<unsigned char> speechMarks;
    if (isSSML) {
        textType = TextType::ssml;
    } else {
        textType = TextType::text;
    }
    String textToBeSent = text;
    ppInternal->getAudioData(textToBeSent, textType, [this, audioResponseRecvd, speechMarks, handler](const char* text, const std::vector<unsigned char> audioDataVal, AWSError<PollyErrors> error) mutable {
        audioResponseRecvd = true;
        speechMarks = audioDataVal;
        this->notifyIfPossible(text, audioDataVal, error.GetMessage().c_str(), handler);
    });
}

void TruPolly::notifyIfPossible(const char *text, std::vector<unsigned char> audioDataVal, const char* error, PollyDataHandler handler) {
    handler(text, audioDataVal, error);
}

void TruPolly::setOutputFormat(Aws::Polly::Model::OutputFormat format) {
    ppInternal->setOutputFormat(format);
}

void TruPolly::setSampleRate(const char *sampleRate) {
    ppInternal->setSampleRate(sampleRate);
}
