// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <stdio.h>
#include <aws/core/Aws.h>
#include <aws/core/client/ClientConfiguration.h>
#include <aws/core/auth/AWSCredentialsProvider.h>
#include <aws/polly/PollyClient.h>
#include <aws/polly/model/SynthesizeSpeechRequest.h>

using namespace Aws::Client;
using namespace Aws::Auth;
using namespace Aws::Polly;
using namespace Aws::Polly::Model;
using Aws::String;

typedef std::function<void(const char* text, const std::vector<unsigned char> audioData, AWSError<PollyErrors> error)> SendTextCompletedHandler;

class TruPollyInternal {
private:
    Aws::SDKOptions options;
    ClientConfiguration *config = NULL;
    AWSCredentials *credentials = NULL;
    PollyClient *pollyClient = NULL;
    
    void createClientConfig();
    void createCredentials();
    void createPollyClient();
    SynthesizeSpeechRequest* getSpeechSynthesisRequest(String text, TextType tType, OutputFormat oFormat, const Aws::Vector<SpeechMarkType>& speechMarkTypes);
    void synthesizeSpeechResponseHandler(const PollyClient *client, const SynthesizeSpeechRequest& request, SynthesizeSpeechOutcome& outcome, const std::shared_ptr<const AsyncCallerContext>& context);
    void OnPollySynthSpeechOutcomeRecieved(const PollyClient* client,
                                           const Model::SynthesizeSpeechRequest& request,
                                           const SynthesizeSpeechOutcome& outcome,
                                           const std::shared_ptr<const AsyncCallerContext>& context) const;
    void setDefaultValues();
    
    OutputFormat selectedOutputFormat;
    const char *selectedSampleRate;
    
public:
    TruPollyInternal();
    ~TruPollyInternal();
    
    ClientConfiguration* getClientConfig();
    AWSCredentials* getCredentials();
    PollyClient* getPollyClient();
    void getAudioData(String text, TextType tType, SendTextCompletedHandler handler);
    void getSpeechMarks(String text, TextType tType, SendTextCompletedHandler handler);
    
    void setOutputFormat(Aws::Polly::Model::OutputFormat format);
    void setSampleRate(const char *sampleRate);
};
