// Fill out your copyright notice in the Description page of Project Settings.

#include "TruPollyInternal.h"
#include <aws/core/Aws.h>
#include <aws/core/utils/Outcome.h>
#include "TruPollyLogger.h"

using std::cout;

static const char* ALLOCATION_TAG = "PollyProvider";

struct SendTextCompletionHandlerCallbackContext : public Aws::Client::AsyncCallerContext {
    SendTextCompletedHandler callback;
};

TruPollyInternal::TruPollyInternal() {
    Aws::InitAPI(options);
    createClientConfig();
    createCredentials();
    createPollyClient();
    setDefaultValues();
}

void TruPollyInternal::setDefaultValues() {
    selectedOutputFormat = OutputFormat::mp3;
    selectedSampleRate = "22050";
}

TruPollyInternal::~TruPollyInternal() {
    Aws::ShutdownAPI(options);
}

void TruPollyInternal::createClientConfig() {
    if (config == NULL) {
        config = new ClientConfiguration();
        config->region = "eu-west-1";
        // TODO : This is added  for testing purpose as of now, as getting error on linux server = unable to connect to endpoint. Check if needed to keep this or remove it, and update as per required.
        config->verifySSL = false;
    }
}

void TruPollyInternal::createCredentials() {
    if (credentials == NULL) {
        credentials = new AWSCredentials();
        credentials->SetAWSSecretKey("oL65+hsHyjDUh7l2cB0PCAujWUh09erGqBC9muq2");
        credentials->SetAWSAccessKeyId("AKIAIBZ2JCOLRYTEHP2Q");
    }
}

void TruPollyInternal::createPollyClient() {
    if (pollyClient == NULL) {
        pollyClient = new PollyClient(*credentials, *config);
    }
}

ClientConfiguration* TruPollyInternal::getClientConfig() {
    return config;
}

AWSCredentials* TruPollyInternal::getCredentials() {
    return credentials;
}

PollyClient* TruPollyInternal::getPollyClient() {
    return pollyClient;
}

void TruPollyInternal::setOutputFormat(Aws::Polly::Model::OutputFormat format) {
    selectedOutputFormat = format;
}

void TruPollyInternal::setSampleRate(const char *sampleRate) {
    selectedSampleRate = sampleRate;
}

void TruPollyInternal::getAudioData(String text, TextType tType, SendTextCompletedHandler handler) {
    Aws::Vector<SpeechMarkType> smTypes = Aws::Vector<SpeechMarkType>();
    SynthesizeSpeechRequest *request = getSpeechSynthesisRequest(text, tType, selectedOutputFormat, smTypes);
    auto context = Aws::MakeShared<SendTextCompletionHandlerCallbackContext>(ALLOCATION_TAG);
    context->callback = handler;
    //    auto self = shared_from_this();
    pollyClient->SynthesizeSpeechAsync(*request, [this](const PollyClient* client,
                                                        const SynthesizeSpeechRequest& request,
                                                        const SynthesizeSpeechOutcome& speechOutcome,
                                                        const std::shared_ptr<const Aws::Client::AsyncCallerContext>& context) {
        TruPollyLogger::log("In synthesizeSpeech response");
        this -> OnPollySynthSpeechOutcomeRecieved(client, request, speechOutcome, context);
    }, context);
}

void TruPollyInternal::getSpeechMarks(String text, TextType tType, SendTextCompletedHandler handler) {
    //    // cout << "Here 1";
    //    Aws::Vector<SpeechMarkType> smTypes = Aws::Vector<SpeechMarkType>();
    //    smTypes.push_back(SpeechMarkType::word);
    //    smTypes.push_back(SpeechMarkType::viseme);
    //
    //    SynthesizeSpeechRequest request = getSpeechSynthesisRequest(text, tType, OutputFormat::json, smTypes);
    //
    //    auto context = Aws::MakeShared<SendTextCompletionHandlerCallbackContext>(ALLOCATION_TAG);
    //    context->callback = handler;
    //
    //    auto self = shared_from_this();
    //    // cout << "Testing";
    //    pollyClient->SynthesizeSpeechAsync(request, [self](const PollyClient* client,
    //                                                       const SynthesizeSpeechRequest& request,
    //                                                       const SynthesizeSpeechOutcome& speechOutcome,
    //                                                       const std::shared_ptr<const Aws::Client::AsyncCallerContext>& context) {
    //        // cout << "Get response";
    //        self -> OnPollySynthSpeechOutcomeRecieved(client, request, speechOutcome, context);
    //    }, context);
    //
    //    // cout << "Here 2";
    //    pollyClient->SynthesizeSpeechAsync(request, <#const SynthesizeSpeechResponseReceivedHandler &handler#>)
}

void TruPollyInternal::OnPollySynthSpeechOutcomeRecieved(const PollyClient* client,
                                                         const SynthesizeSpeechRequest& request,
                                                         const SynthesizeSpeechOutcome& outcome,
                                                         const std::shared_ptr<const AsyncCallerContext>& context) const {
    TruPollyLogger::log("In OnPollySynthSpeechOutcomeRecieved");
    auto callback = ((const std::shared_ptr<SendTextCompletionHandlerCallbackContext>&)context)->callback;
    
    if(outcome.IsSuccess()) {
        TruPollyLogger::log("Received success");
        SynthesizeSpeechResult result = const_cast<SynthesizeSpeechOutcome&>(outcome).GetResultWithOwnership();
        if (strcmp(result.GetContentType().c_str(), "audio/json") == 0) {
            // We have received result for speech marks.
            TruPollyLogger::log("content is json");
        } else {
            // We have received result for audio.
            TruPollyLogger::log("Content is audio");
            Aws::IOStream& stream = result.GetAudioStream();
            
            typedef std::vector<unsigned char> buffer_type;
            buffer_type myData;
            
            while (stream) {
                int BufferSize = 1024;
                unsigned char buffer[BufferSize];
                stream.read((char *)buffer, BufferSize);
                myData.insert(myData.end(), buffer, buffer + BufferSize);
            }
            
            if (callback) {
                callback(request.GetText().c_str(), myData, outcome.GetError());
            }
        }
    } else {
        TruPollyLogger::log("speechSynthesis error : " + string(outcome.GetError().GetMessage().c_str()));
        if (callback) {
            typedef std::vector<unsigned char> buffer_type;
            buffer_type myData;
            callback(request.GetText().c_str(), myData, outcome.GetError());
        }
        // cout << "Error";
        // cout << "Error while fetching audio from polly. " << outcome.GetError().GetExceptionName() << " " << outcome.GetError().GetMessage();
    }
}

void TruPollyInternal::synthesizeSpeechResponseHandler(const PollyClient *client, const SynthesizeSpeechRequest& request, SynthesizeSpeechOutcome& outcome, const std::shared_ptr<const AsyncCallerContext>& context) {
}

SynthesizeSpeechRequest* TruPollyInternal::getSpeechSynthesisRequest(String text, TextType tType, OutputFormat oFormat, const Aws::Vector<SpeechMarkType>& speechMarkTypes) {
    SynthesizeSpeechRequest *request = new SynthesizeSpeechRequest();
    request->SetSampleRate(String(selectedSampleRate));
    request->SetTextType(tType);
    request->SetOutputFormat(oFormat);
    request->SetText(text);
    request->SetVoiceId(VoiceId::Amy);
    if (speechMarkTypes.capacity() > 0) {
        request->SetSpeechMarkTypes(speechMarkTypes);
    }
    return request;
}
